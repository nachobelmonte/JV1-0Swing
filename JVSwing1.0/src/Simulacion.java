import java.awt.Color;

/**
 * Clase que se encarga del movimiento de las celulas
 * 
 * @author Ignacio Belmonte
 *
 */
public class Simulacion {

	private byte[][] mundo;

	// Reglas del juego
	final int REVIVIR = 3;
	final int MANTENER_VIVA = 2;

	public Simulacion() {

		mundo = new byte[Principal.PanelMain.espacioMundo.length][Principal.PanelMain.espacioMundo[0].length];

	}

	/**
	 * Arranca la demo
	 */
	public void arrancarDemo() {

		do{
			cargarMundoDemo();
			try {
				Thread.sleep(70);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			actualizarMundo();

			try {
				Thread.sleep(70);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mostrarMundo();
			try {
				Thread.sleep(70);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}while(true);
	}

	/**
	 * Muestra el mundo actual por pantalla
	 */
	private void mostrarMundo() {

		for (int i = 0; i < mundo.length; i++) {

			for (int j = 0; j < mundo[i].length; j++) {

				if (mundo[i][j] == 1) {

					Principal.PanelMain.espacioMundo[i][j].setBackground(Color.BLACK);

				} else {

					Principal.PanelMain.espacioMundo[i][j].setBackground(Color.WHITE);
					;
				}
			}

		}
	}

	/**
	 * Actualiza una unidad de tiempo el mundo
	 */
	private void actualizarMundo() {

		byte[][] mundoAux = new byte[mundo.length][mundo[0].length];

		for (int i = 0; i < mundo.length; i++) {
			for (int j = 0; j < mundo[i].length; j++) {

				int numVecinosVivos = numVecinosVivos(i, j);
				// Revivir celula
				if (numVecinosVivos == REVIVIR) {
					mundoAux[i][j] = 1;
				}
				// Mantener viva
				if (numVecinosVivos == MANTENER_VIVA) {
					mundoAux[i][j] = mundo[i][j];
				}
				// Matar celula
				if (numVecinosVivos != REVIVIR && numVecinosVivos != MANTENER_VIVA) {
					mundoAux[i][j] = 0;
				}
			}
		}
		mundo = mundoAux;
	}

	/**
	 * Carga las celulas vivas iniciales
	 */
	private void cargarMundoDemo() {

		for (int i = 0; i < Principal.PanelMain.espacioMundo.length; i++) {
			for (int k = 0; k < Principal.PanelMain.espacioMundo.length; k++) {
				if (Principal.PanelMain.espacioMundo[i][k].getBackground().equals(Color.BLACK)) {
					mundo[i][k] = 1;
				}
			}
		}

	}

	/**
	 * Calcula numero de vecinos vivos
	 * 
	 * @param coordenadasX
	 * @param coordenadasY
	 * @return numero de vecinos vivos
	 */
	private int numVecinosVivos(int coordenadasX, int coordenadasY) {

		int numVecinosVivos = 0;
		byte[] vecinos = caclularVecinos(coordenadasX, coordenadasY);
		for (int i = 0; i < vecinos.length; i++) {

			numVecinosVivos += vecinos[i];
		}
		return numVecinosVivos;
	}

	/**
	 * Cacula los vecinos de una celula
	 * 
	 * @param coordenadasX
	 * @param coordenadasY
	 * @return array con los valores de sus vecinos
	 */
	private byte[] caclularVecinos(int coordenadasX, int coordenadasY) {
		byte[] vecinos = new byte[8];

		/**
		 * Configuracion movimientos array para que sea un mundo infinito
		 */
		int derecha = coordenadasY != mundo[coordenadasX].length - 1 ? coordenadasY + 1 : 0;
		int izquierda = coordenadasY != 0 ? coordenadasY - 1 : mundo[coordenadasX].length - 1;
		int arriba = coordenadasX != 0 ? coordenadasX - 1 : mundo.length - 1;
		int abajo = coordenadasX != mundo.length - 1 ? coordenadasX + 1 : 0;

		vecinos[0] = mundo[coordenadasX][derecha];
		vecinos[1] = mundo[coordenadasX][izquierda];
		vecinos[2] = mundo[arriba][derecha];
		vecinos[3] = mundo[arriba][coordenadasY];
		vecinos[4] = mundo[arriba][izquierda];
		vecinos[5] = mundo[abajo][derecha];
		vecinos[6] = mundo[abajo][izquierda];
		vecinos[7] = mundo[abajo][coordenadasY];

		return vecinos;
	}

}