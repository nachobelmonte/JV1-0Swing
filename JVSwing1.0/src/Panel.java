
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.JButton;

import java.awt.FlowLayout;

/**
 * Clase que genera el panel grafico
 * 
 * @author Ignacio Belmonte
 *
 */
public class Panel {

	public JButton espacioMundo[][] = new JButton[20][20];
	public JFrame frame;

	// Accion para cuando se pulsa una celula
	ActionListener cambiarColor = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			JButton botonEvento = (JButton) e.getSource();
			if (botonEvento.getBackground().equals(Color.WHITE)) {
				botonEvento.setBackground(Color.BLACK);
			} else {
				botonEvento.setBackground(Color.WHITE);
			}
		}
	};



	/**
	 * Constructor
	 */
	public Panel() {
		initialize();
	}

	/**
	 * Inicia el contenido del frame
	 */
	public void initialize() {

		frame = new JFrame("El juego de la vida");
		frame.setBounds(200, 200, 450, 450);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		/**
		 * Panel que contiene el tablero del juego
		 */
		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(new GridLayout(20, 20, 1, 1));

		for (int x = 0; x < espacioMundo.length; x++) {
			for (int y = 0; y < espacioMundo[x].length; y++) {

				espacioMundo[x][y] = new JButton();
				panel.add(espacioMundo[x][y]);
				espacioMundo[x][y].setBackground(Color.WHITE);
				espacioMundo[x][y].addActionListener(cambiarColor);

			}
		}

		/**
		 * Panel que contiene los botones
		 */
		JPanel panel_1 = new JPanel();
		frame.getContentPane().add(panel_1, BorderLayout.PAGE_END);
		panel_1.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		/**
		 * Boton play
		 */
		JButton btnPlay = new JButton("Play");
		btnPlay.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				Principal.iniciarHilo();

			}
		});
		panel_1.add(btnPlay);

		/**
		 * Boton stop
		 */
		JButton btnStop = new JButton("Stop");
		btnStop.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				Principal.pararHilo();

			}
		});
		panel_1.add(btnStop);
	}

}// class
