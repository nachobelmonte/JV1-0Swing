import java.awt.EventQueue;
import java.util.ArrayList;


/**
 * Clase principal
 * 
 * @author Ignacio Belmonte
 *
 */
public class Principal {

	static Panel PanelMain = new Panel();
	static int numeroHilo = -1;
	//static Thread[] movimientoCelulas = new Thread[10000];
	static ArrayList<Thread> movimientoCelulas = new ArrayList<Thread>();

	public static void main(String[] args) {

		// Inicia el panel
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PanelMain.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

	/**
	 * Inicia un nuevo hilo de ejecucion
	 */
	@SuppressWarnings("deprecation")
	public static void iniciarHilo() {
		numeroHilo++;

		// Se crea un nuevo hilo
		movimientoCelulas.add(numeroHilo,(new Thread(new Runnable() {

			@Override
			public void run() {

				Simulacion simulacion = new Simulacion();
				simulacion.arrancarDemo();
			}
		})));

		try {
			if (numeroHilo > 0) {
				// Se para el hilo anterior
				movimientoCelulas.get(numeroHilo-1).stop();
			}
			// Se inicia el nuevo hilo creado
			movimientoCelulas.get(numeroHilo).start();

		} catch (Exception ef) {

		}
	}

	/**
	 * Para el hilo de ejcucion
	 */
	@SuppressWarnings("deprecation")
	public static void pararHilo() {
		try {
			movimientoCelulas.get(numeroHilo).stop();

		} catch (Exception ef) {

		}
	}

}
